import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class CivicSpaceZipcodeFileParser{

	//set filterByZips = null to get the full list if wanted
	public static ArrayList<CivicSpaceZipcodeData> parseFile(String filename, boolean includeInvalidItems, String[] filterByZips) throws FileNotFoundException, IOException{
		BufferedReader reader = null;
		ArrayList<CivicSpaceZipcodeData> dataList = new ArrayList<CivicSpaceZipcodeData>();

		try{
			reader = new BufferedReader(new FileReader(filename));
			String line = null;
			//discard the first line because it just contains the headers
			line = reader.readLine();

			while(line != null){
				line = reader.readLine();

				if(line != null){
					line = line.trim();

					//just discard any blank lines
					if(line.length() != 0){
						CivicSpaceZipcodeData dataItem = new CivicSpaceZipcodeData(line);

						if(dataItem.isValid() || includeInvalidItems){

							if(filterByZips == null){
								dataList.add(dataItem);
							}
							else{

								for(int i = 0; i < filterByZips.length; i++){

									if(dataItem.getZip() != null && dataItem.getZip().equals(filterByZips[i])){
										dataList.add(dataItem);
										//it matches at least one of the filterByZips so exit the for loop
										break;
									}

								}

							}

						}

					}

				}
			}

		}
		finally{

			try{
				if(reader != null) reader.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

		}

		return dataList;		
	}

}