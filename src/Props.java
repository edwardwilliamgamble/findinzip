import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;

public class Props extends Properties{
	private static Props p = new Props();
/*
	static{
		p = new Props();
	}
*/
	public Props(){
		super();
		init();
	}
	
	private void init(){

		try {
			load(new FileInputStream("config.properties"));
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}	

	}

	public static Props getProps(){
		return p;
	}

}