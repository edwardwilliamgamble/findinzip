public class LatLonData{
	private String inputString = null;
	private String latS = null;
	private String lonS = null;
	private float latF = -9999.0f;
	private float lonF = -9999.0f;
	private boolean isValid = false; 

	public LatLonData(String inputString, int latCol, int lonCol){
		parse(inputString, latCol, lonCol);
	}

	public void parse(String inputString, int latCol, int lonCol){
		isValid = false;

		if(inputString != null){
			this.inputString = inputString;
			String[] tokens = inputString.split(",");

			if(tokens.length > latCol){
				this.latS = tokens[latCol].trim();
			}
			else{
				this.latS = "";
			}

			if(tokens.length > lonCol){
				this.lonS = tokens[lonCol].trim();
			}
			else{
				this.lonS = "";
			}

			parse(this.latS, this.lonS);
		}

	}

	public LatLonData(String latS, String lonS){
		parse(latS, lonS);
	}

	public void parse(String latS, String lonS){

		if(latS == null){
			this.isValid = false;
			return;
		}

		if(lonS == null){
			this.isValid = false;
			return;
		}

		this.latS = latS.trim();
		this.lonS = lonS.trim();
		this.isValid = true;

		try{
			this.latF = Float.parseFloat(this.latS);
			this.lonF = Float.parseFloat(this.lonS);
		}
		catch(NumberFormatException nfe){
			this.isValid = false;
		}

		if(this.isValid && 
			Float.isNaN(this.latF)
			|| Float.isInfinite(this.latF) 
			|| this.latF < -90.0f 
			|| this.latF > 90.0f
			|| Float.isNaN(this.lonF)
			|| Float.isInfinite(this.lonF) 
			|| this.lonF < -180.0f
			|| this.lonF > 180.0f
		){
			this.isValid = false;
		}

	}

	public boolean isValid(){
		return isValid;
	}

	public String getInputString(){
		return inputString;
	}

	public String getLatS(){
		return latS;
	}

	public String getLonS(){
		return lonS;
	}

	public float getLatF(){
		return latF;
	}

	public float getLonF(){
		return lonF;
	}

	public String toString(){
		String ret = "";

		if(isValid){
			ret = ""+this.latS+","+this.lonS;
		}
		else if(inputString != null){
			ret = "<Invalid location data item:"+this.inputString+">";
		}
		else{
			ret = "<Invalid location data item with no input values>";
		}

		return ret;
	}

}
