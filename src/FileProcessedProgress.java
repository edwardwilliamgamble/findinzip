import java.math.BigDecimal;

public class FileProcessedProgress{
	private long linesMatched = 0l;
	private long linesCouldNotParse = 0l;
	private long totalNumberOfLinesProcessed = 0l;
	private long fileLineCount = -1l;

	public FileProcessedProgress(FileProcessedResultsSummary summ){
		this.linesMatched = summ.linesMatched;
		this.linesCouldNotParse = summ.linesCouldNotParse;
		this.totalNumberOfLinesProcessed = summ.totalNumberOfLinesProcessed;
		this.fileLineCount = summ.fileLineCount;
	}

	public FileProcessedProgress(long linesMatched, long linesCouldNotParse, long totalNumberOfLinesProcessed, long fileLineCount){
		this.linesMatched = linesMatched;
		this.linesCouldNotParse = linesCouldNotParse;
		this.totalNumberOfLinesProcessed = totalNumberOfLinesProcessed;
		this.fileLineCount = fileLineCount;
	}

	public long getLinesMatched(){
		return linesMatched;
	}

	public long getLinesCouldNotParse(){
		return linesCouldNotParse;
	}

	public long getTotalNumberOfLinesProcessed(){
		return totalNumberOfLinesProcessed;
	}

	public long getFileLineCount(){
		return fileLineCount;
	}

	public String toString(){

		if(fileLineCount < 0){
			return "Lines processed: "+totalNumberOfLinesProcessed+", "+ getCompletedPercentDesc(totalNumberOfLinesProcessed,fileLineCount) +"%, Matched: "+linesMatched+", Malformed input lines: "+linesCouldNotParse;
		}
		else{
			return "Lines processed: "+totalNumberOfLinesProcessed+" / " + fileLineCount + ", "+ getCompletedPercentDesc(totalNumberOfLinesProcessed,fileLineCount) +"%, Matched: "+linesMatched+", Malformed input lines: "+linesCouldNotParse;
		}

	}

	public static String getCompletedPercentDesc(long numer, long denom){
		BigDecimal dn = new BigDecimal(""+numer);
		BigDecimal dd = new BigDecimal(""+denom);
		BigDecimal bret = dn.multiply(new BigDecimal("100.00")).divide(dd, BigDecimal.ROUND_UP);
		float f = bret.floatValue();

		//don't show 100.00% until we are completely done
		if(f == 100.00 && numer < denom) f = 99.99f;

		return String.format("%6.2f",f);
	}

}