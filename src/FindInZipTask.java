import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.util.concurrent.*;

//the SwingWorker class guarantees that doInBackground() will only be executed once per instance of the class
public class FindInZipTask extends SwingWorker<FileProcessedResultsSummary, FileProcessedProgress> {
	boolean cont = true;
	Gui gui;
	String inputFilename;
	int latCol;
	int lonCol;
	String outputFilename;
	String inputZipS;
		
	FindInZipTask(Gui gui, String inputFilename, String latColS, String lonColS, String outputFilename, String inputZipS) {
		this.gui = gui;
		this.inputFilename = inputFilename;
		this.outputFilename = outputFilename;
		this.inputZipS = inputZipS;

		try{
			this.latCol = Integer.parseInt(latColS);
		}
		catch(NumberFormatException nfe){
			this.latCol = 0;
		}

		try{
			this.lonCol = Integer.parseInt(lonColS);
		}
		catch(NumberFormatException nfe){
			this.lonCol = 1;
		}

		if(this.latCol < 0) this.latCol = 0;
		if(this.lonCol < 0) this.lonCol = 0;
		if(this.latCol == this.lonCol) this.lonCol = this.latCol + 1;
	}

	//sets the cont variable that the running process checks in a loop to see if it should stop
	public void setCont(boolean cont){
		this.cont = cont;
	}

	//gets the cont variable that the running process checks in a loop to see if it should stop
	public boolean getCont(){
		return cont;
	}

	protected void publ(FileProcessedProgress prog){
		publish(prog);
	}

	//executes on a worker thread
	@Override
	public FileProcessedResultsSummary doInBackground(){
		FileProcessedResultsSummary summ = new FileProcessedResultsSummary();
		summ.status = FileProcessedResultsSummary.ErrorCode.IN_PROGRESS;
		this.publ(new FileProcessedProgress(summ));
		return LatLonFileParser.parseFileFindWithinZipcodeWriteOut(inputFilename, latCol, lonCol, outputFilename, inputZipS, this);
	}

	//executes on the swing event-dispatching thread:
	//called automatically when the doInBackground code has completed and the task is done, or if it was canceled.  Use get method to get the results.
	//used to update the gui based on prgress feedback from the running task
	@Override
	protected void done() {
		gui.cancelButton.setEnabled(false);
		FileProcessedResultsSummary summ = null;

		try{
			summ = this.get();
		}
		catch(InterruptedException ie){
			System.out.println(ie);
			ie.printStackTrace();
		}
		catch(ExecutionException ee){
			System.out.println(ee);
			ee.printStackTrace();
		}

		if(summ.status == FileProcessedResultsSummary.ErrorCode.NOT_STARTED){
		}
		else if(summ.status == FileProcessedResultsSummary.ErrorCode.IN_PROGRESS){
		}
		else if(summ.status == FileProcessedResultsSummary.ErrorCode.CANCELED){
			JOptionPane opt = new JOptionPane("Canceled: "+summ.toString());
			JDialog dialog = opt.createDialog(gui.frame, "Canceled!");
			dialog.setVisible(true);
		}
		else if(summ.status == FileProcessedResultsSummary.ErrorCode.SUCCESS){
			JOptionPane opt = new JOptionPane("Success: "+summ.toString());
			JDialog dialog = opt.createDialog(gui.frame, "Done!");
			dialog.setVisible(true);
		}
		else if(summ.status == FileProcessedResultsSummary.ErrorCode.FATAL_ERROR){
			JOptionPane opt = new JOptionPane("ERROR: "+summ.fatalErrorDetails);
			JDialog dialog = opt.createDialog(gui.frame, "Error!");
			dialog.setVisible(true);
		}

		gui.runButton.setEnabled(true);
			
	}

	//executes on the swing event-dispatching thread:
	//Receives data chunks from the publish method asynchronously on the Event Dispatch Thread
	//used to update the gui based on prgress feedback from the running task
	@Override
	protected void process(java.util.List<FileProcessedProgress> chunks){
			
		if(chunks != null && chunks.size() > 0){
			FileProcessedProgress latestProgress = chunks.get(chunks.size()-1);
			gui.frame.setTitle("Find In Zip  "+latestProgress.toString());
		}

	}

}
