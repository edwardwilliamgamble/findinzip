import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.ArrayList;

import java.io.File;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.RenderingHints;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import org.geotools.data.DataUtilities;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.memory.MemoryFeatureCollection;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapViewport;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.SchemaException;
import org.geotools.feature.collection.SubFeatureCollection;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.factory.Hints;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;

import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.Property;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;


public class GeoUtil{
	private static final String ICON_IMAGE_FILENAME = ".\\icon.png";
	private static final String WORLD_OUTLINE_FILENAME = "./data/110m_physical/ne_110m_coastline.shp";
	private static final String ZCTA_SHP_FILENAME = "tl_2012_us_zcta510/tl_2012_us_zcta510.shp";
	private static final String ZIP_ATTR_NAME = "GEOID10"; // or "ZCTA5CE10"
	private static final String LAT_ATTR_NAME = "INTPTLAT10";
	private static final String LON_ATTR_NAME = "INTPTLON10";
	private static final CoordinateReferenceSystem DEFAULT_CRS = DefaultGeographicCRS.WGS84;



	public static FileProcessedResultsSummary matchWithBoundaryZcta(
			String inputFilename, int latCol, int lonCol, String outputFilename, 
			String inputZipS, double maxMatchDistanceInMi, 
			FindInZipTask task){
		long linesLastUpdatedAt = 0;
		FileProcessedResultsSummary res = new FileProcessedResultsSummary();
		res.status = FileProcessedResultsSummary.ErrorCode.IN_PROGRESS;
		BufferedReader reader = null;
		ArrayList<LatLonData> positiveResults = new ArrayList<LatLonData>();
		ArrayList<LatLonData> negativeResults = new ArrayList<LatLonData>();

		boolean mapPosRes = ("true".equalsIgnoreCase(Props.getProps().getProperty("mapPositiveResults", "false")));
		boolean mapNegRes = ("true".equalsIgnoreCase(Props.getProps().getProperty("mapNegativeResults", "false")));

		inputZipS = inputZipS.trim();
		MemoryFeatureCollection zipCollection = null;

		try{
			zipCollection = loadZctaShape(inputZipS);
		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error reading from ZCTA zipcode shape file: "+ ZCTA_SHP_FILENAME + "! " + ioe;
			return res;
		}

		if(zipCollection == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Unknown zipcode! "+ inputZipS;
			return res;	
		}

		SimpleFeature zipFeat = null;
		SimpleFeatureIterator memIter = zipCollection.features();

		try{
			zipFeat = memIter.next();
		}
		finally{
			memIter.close();
		}

		MultiPolygon zipPoly = null;

		if(zipFeat != null && zipFeat.getDefaultGeometry() instanceof MultiPolygon){
			zipPoly = (MultiPolygon)zipFeat.getDefaultGeometry();
		}

		if(zipCollection == null || zipPoly == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Unknown zipcode! "+ inputZipS;
			return res;
		}

		try{
			res.fileLineCount = LatLonFileParser.countLinesInFile(inputFilename);
		}
		catch(FileNotFoundException fnfe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file to count lines: "+ inputFilename + "! " + fnfe;
			return res;
		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file to count lines: "+ inputFilename + "! " + ioe;
			return res;
		}

		try{
			reader = new BufferedReader(new FileReader(inputFilename));
		}
		catch(FileNotFoundException fnfe){

			try{
				if(reader != null)reader.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file: "+ inputFilename + "! " + fnfe;
			return res;
		}

		BufferedWriter writer = null;

		try{
			writer = new BufferedWriter(new FileWriter(outputFilename));
		}
		catch(IOException ioe){

			try{
				if(writer != null)writer.close();
			}
			catch(IOException cioe){
				System.out.println(cioe);
			}

			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening output stream to output file: "+ outputFilename + "! " + ioe;
			return res;
		}		

		Hints hints = new Hints(Hints.DEFAULT_COORDINATE_REFERENCE_SYSTEM, DEFAULT_CRS);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(hints);
		SimpleFeatureType pointType = null;

		try{
			pointType = DataUtilities.createType("mytype","the_geom:Point, id:String");
		}
		catch(SchemaException se){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = ""+se;
			return res;
		}

		try{
			String line = null;

			//skip the first line that contains the headers
			line = reader.readLine();
			res.totalNumberOfLinesProcessed++;

			//write header out to output file
			if(line != null) writer.write(line+'\n');

			while(line != null && task.getCont()){
				line = reader.readLine();

				if(line != null){
					res.totalNumberOfLinesProcessed++;
					line = line.trim();

					//just discard any blank lines
					if(line.length() != 0){
						LatLonData dataItem = new LatLonData(line, latCol, lonCol);

						if(dataItem.isValid()){
							Coordinate coord = new Coordinate(dataItem.getLonF(), dataItem.getLatF());
							Point point = geometryFactory.createPoint(coord);

							if(point.intersects(zipPoly)){
								writer.write(dataItem.getInputString()+'\n');
								res.linesMatched++;

								if(mapPosRes){
									positiveResults.add(dataItem);
								}

							}
							else{

								if(mapNegRes){
									negativeResults.add(dataItem);
								}

							}

						}
						else{
							res.linesCouldNotParse++;
						}

					}

					if(res.totalNumberOfLinesProcessed > linesLastUpdatedAt+LatLonFileParser.UPDATE_LINES_NUM){
						task.publ(new FileProcessedProgress(res));
					}

				}

			}


		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error reading from input file: "+ inputFilename + "! " + ioe;
			return res;
		}		
		finally{

			try{
				if(reader != null)reader.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

			try{
				if(writer != null)writer.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

		}

		res.status = FileProcessedResultsSummary.ErrorCode.SUCCESS;

		if(!task.getCont()){
			res.status = FileProcessedResultsSummary.ErrorCode.CANCELED;
		}

		task.publ(new FileProcessedProgress(res));

		if(mapPosRes||mapNegRes){
			mapResults(positiveResults, negativeResults, zipCollection);
		}

		return res;
	}


	private static MemoryFeatureCollection loadZctaShape(String inputZipS) throws IOException{
		File file = new File(ZCTA_SHP_FILENAME);
	        FileDataStore store = FileDataStoreFinder.getDataStore(file);
        	SimpleFeatureSource featureSource = store.getFeatureSource();
		FilterFactory ff = CommonFactoryFinder.getFilterFactory(null);
		Filter filter = ff.equals(ff.property(ZIP_ATTR_NAME), ff.literal(inputZipS));
		SimpleFeatureCollection featureCollection = featureSource.getFeatures(filter);
		MemoryFeatureCollection memoryFeatureCollection = new MemoryFeatureCollection(featureCollection.getSchema());
		SimpleFeatureIterator featureIter = featureCollection.features();
		boolean found = false;

		try{
			while(featureIter.hasNext()){
				SimpleFeature feature = featureIter.next();

				if(feature != null){
					Object zipObj = feature.getAttribute(ZIP_ATTR_NAME);
					Object latObj = feature.getAttribute(LAT_ATTR_NAME);
					Object lonObj = feature.getAttribute(LON_ATTR_NAME);
					Object geomObj = feature.getDefaultGeometry();

					String zipS = null;
					String latS = null;
					String lonS = null;
					MultiPolygon multiPoly = null;

					if(zipObj != null && zipObj instanceof String){

						if(latObj != null && latObj instanceof String){

							if(lonObj != null && lonObj instanceof String){

								if(geomObj != null && geomObj instanceof MultiPolygon){
									zipS = (String)	zipObj;
									latS = (String)	latObj;
									lonS = (String)	lonObj;
									multiPoly = (MultiPolygon) geomObj;

									if(inputZipS.equals(zipS)){
										memoryFeatureCollection.add(feature);
										found = true;
										break;
									}							

								}

							}

						}

					}

				}

			}

		}
		finally{
			featureIter.close();		
		}

		if(!found) return null;
		return memoryFeatureCollection;
	}

	public static void mapResults(ArrayList<LatLonData> positiveResults, ArrayList<LatLonData> negativeResults, MemoryFeatureCollection zipCollection){
		SimpleFeatureType pointType = null;

		try{
			pointType = DataUtilities.createType("mytype","the_geom:Point, id:String");
		}
		catch(SchemaException se){
			System.out.println(se);
			return;
		}

		MemoryFeatureCollection hitPointMemoryFeatureCollection = new MemoryFeatureCollection(pointType);
		MemoryFeatureCollection missPointMemoryFeatureCollection = new MemoryFeatureCollection(pointType);
		CoordinateReferenceSystem crs = DefaultGeographicCRS.WGS84;
		Hints hints = new Hints(Hints.DEFAULT_COORDINATE_REFERENCE_SYSTEM, crs);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory( hints );

		for(int i = 0; i < positiveResults.size(); i++){
			LatLonData ll = positiveResults.get(i);
			Coordinate coord = new Coordinate(ll.getLonF(), ll.getLatF());
			Point point = geometryFactory.createPoint( coord );
			SimpleFeature pointFeature = SimpleFeatureBuilder.build(pointType, new Object[]{point, "1"}, null);
			hitPointMemoryFeatureCollection.add(pointFeature);
		}

		for(int i = 0; i < negativeResults.size(); i++){
			LatLonData ll = negativeResults.get(i);
			Coordinate coord = new Coordinate(ll.getLonF(), ll.getLatF());
			Point point = geometryFactory.createPoint( coord );
			SimpleFeature pointFeature = SimpleFeatureBuilder.build(pointType, new Object[]{point, "1"}, null);
			missPointMemoryFeatureCollection.add(pointFeature);
		}

		MapContent map = new MapContent();
		map.setTitle("View Results");
		File worldOutlineFile = new File(WORLD_OUTLINE_FILENAME);

		try{
			FileDataStore worldOutlineStore = FileDataStoreFinder.getDataStore(worldOutlineFile);
			SimpleFeatureSource worldOutlineFeatureSource = worldOutlineStore.getFeatureSource();
			Style worldOutlineStyle = SLD.createSimpleStyle(worldOutlineFeatureSource.getSchema(), new Color(0.8f, 0.8f, 0.8f));
			Layer worldOutlineLayer = new FeatureLayer(worldOutlineFeatureSource, worldOutlineStyle);
	        	map.addLayer(worldOutlineLayer);
		}
		catch(IOException ioe){
			ioe.printStackTrace();
		}

	        Style zipStyle = SLD.createSimpleStyle(zipCollection.getSchema(), new Color(1.0f, 0.0f, 1.0f));
        	Layer zipLayer = new FeatureLayer(zipCollection, zipStyle);
	        map.addLayer(zipLayer);

		Style missPointStyle = SLD.createSimpleStyle(missPointMemoryFeatureCollection.getSchema(), Color.RED);
        	Layer missPointLayer = new FeatureLayer(missPointMemoryFeatureCollection, missPointStyle);
	        map.addLayer(missPointLayer);

	        Style hitPointStyle = SLD.createSimpleStyle(hitPointMemoryFeatureCollection.getSchema(), new Color(0.0f, 0.7f, 0.0f));
        	Layer hitPointLayer = new FeatureLayer(hitPointMemoryFeatureCollection, hitPointStyle);
	        map.addLayer(hitPointLayer);

		JMapFrame mapFrame = new JMapFrame(map);
		mapFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		HashMap<RenderingHints.Key, Object> renderHintsMap = new HashMap<RenderingHints.Key, Object>();
		renderHintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		RenderingHints renderHints = new RenderingHints(renderHintsMap);
		mapFrame.getMapPane().getRenderer().setJava2DHints(renderHints);
		//mapFrame.enableStatusBar(true);
		mapFrame.enableToolBar(true);
		//mapFrame.enableLayerTable(true);
		mapFrame.enableStatusBar(false);
		//mapFrame.enableToolBar(false);
		mapFrame.enableLayerTable(false);
		//mapFrame.initComponents();
		//mapFrame.pack();
		mapFrame.setSize(600,400);
		mapFrame.setLocation(150,150);
		ImageIcon img = new ImageIcon(ICON_IMAGE_FILENAME);
		mapFrame.setIconImage(img.getImage());
        	mapFrame.setVisible(true);	
	}

	public static void mapResults(ArrayList<LatLonData> positiveResults, ArrayList<LatLonData> negativeResults){
		SimpleFeatureType pointType = null;

		try{
			pointType = DataUtilities.createType("mytype","the_geom:Point, id:String");
		}
		catch(SchemaException se){
			System.out.println(se);
			return;
		}

		MemoryFeatureCollection hitPointMemoryFeatureCollection = new MemoryFeatureCollection(pointType);
		MemoryFeatureCollection missPointMemoryFeatureCollection = new MemoryFeatureCollection(pointType);
		CoordinateReferenceSystem crs = DefaultGeographicCRS.WGS84;
		Hints hints = new Hints(Hints.DEFAULT_COORDINATE_REFERENCE_SYSTEM, crs);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory( hints );

		for(int i = 0; i < positiveResults.size(); i++){
			LatLonData ll = positiveResults.get(i);
			Coordinate coord = new Coordinate(ll.getLonF(), ll.getLatF());
			Point point = geometryFactory.createPoint( coord );
			SimpleFeature pointFeature = SimpleFeatureBuilder.build(pointType, new Object[]{point, "1"}, null);
			hitPointMemoryFeatureCollection.add(pointFeature);
		}

		for(int i = 0; i < negativeResults.size(); i++){
			LatLonData ll = negativeResults.get(i);
			Coordinate coord = new Coordinate(ll.getLonF(), ll.getLatF());
			Point point = geometryFactory.createPoint( coord );
			SimpleFeature pointFeature = SimpleFeatureBuilder.build(pointType, new Object[]{point, "1"}, null);
			missPointMemoryFeatureCollection.add(pointFeature);
		}

		MapContent map = new MapContent();
		map.setTitle("View Results");
		File worldOutlineFile = new File(WORLD_OUTLINE_FILENAME);

		try{
			FileDataStore worldOutlineStore = FileDataStoreFinder.getDataStore(worldOutlineFile);
			SimpleFeatureSource worldOutlineFeatureSource = worldOutlineStore.getFeatureSource();
			Style worldOutlineStyle = SLD.createSimpleStyle(worldOutlineFeatureSource.getSchema(), new Color(0.8f, 0.8f, 0.8f));
			Layer worldOutlineLayer = new FeatureLayer(worldOutlineFeatureSource, worldOutlineStyle);
	        	map.addLayer(worldOutlineLayer);
		}
		catch(IOException ioe){
			ioe.printStackTrace();
		}

		Style missPointStyle = SLD.createSimpleStyle(missPointMemoryFeatureCollection.getSchema(), Color.RED);
        	Layer missPointLayer = new FeatureLayer(missPointMemoryFeatureCollection, missPointStyle);
	        map.addLayer(missPointLayer);

	        Style hitPointStyle = SLD.createSimpleStyle(hitPointMemoryFeatureCollection.getSchema(), new Color(0.0f, 0.7f, 0.0f));
        	Layer hitPointLayer = new FeatureLayer(hitPointMemoryFeatureCollection, hitPointStyle);
	        map.addLayer(hitPointLayer);

		JMapFrame mapFrame = new JMapFrame(map);
		mapFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		HashMap<RenderingHints.Key, Object> renderHintsMap = new HashMap<RenderingHints.Key, Object>();
		renderHintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		RenderingHints renderHints = new RenderingHints(renderHintsMap);
		mapFrame.getMapPane().getRenderer().setJava2DHints(renderHints);
		//mapFrame.enableStatusBar(true);
		mapFrame.enableToolBar(true);
		//mapFrame.enableLayerTable(true);
		mapFrame.enableStatusBar(false);
		//mapFrame.enableToolBar(false);
		mapFrame.enableLayerTable(false);
		//mapFrame.initComponents();
		//mapFrame.pack();
		mapFrame.setSize(600,400);
		mapFrame.setLocation(150,150);
		ImageIcon img = new ImageIcon(ICON_IMAGE_FILENAME);
		mapFrame.setIconImage(img.getImage());
        	mapFrame.setVisible(true);	
	}

}