import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.ArrayList;

public class LatLonFileParser{
	protected static final int UPDATE_LINES_NUM = 10000;
	private static final String ZIPCODE_FILENAME = "./zipcodedata/zipcode.csv";
	private static final String MATCH_MODE_CLOSEST_CIV_SPACE_ZIP_CENTER = "closestCivSpaceZipCenter";
	private static final String MATCH_MODE_DISTANCE_TO_CIV_SPACE_ZIP_CENTER = "distanceToCivSpaceZipCenter";
	private static final String MATCH_MODE_ZCTA_ZIP_BOUNDARY_SHAPE = "zctaZipBoundaryShape";
	private static final String MAX_MATCH_DISTANCE_IN_MI = "maxMatchDistanceInMiles";
	private static final String MAX_MATCH_DISTANCE_IN_MI_DEFAULT_S = "-1.0";
	private static final double MAX_MATCH_DISTANCE_IN_MI_DEFAULT = -1.0;

	//only compares with valid Lat/Lons found in file, invalid ones are ignored
	public static FileProcessedResultsSummary parseFileFindWithinZipcodeWriteOut(
			String inputFilename, int latCol, int lonCol, String outputFilename, 
			String inputZipS,
			FindInZipTask task){
		String matchMode = Props.getProps().getProperty("matchMode", MATCH_MODE_CLOSEST_CIV_SPACE_ZIP_CENTER);
		String maxMatchDistanceMode = Props.getProps().getProperty("matchMode", MATCH_MODE_CLOSEST_CIV_SPACE_ZIP_CENTER);
		String maxMatchDistanceInMilesS = Props.getProps().getProperty(MAX_MATCH_DISTANCE_IN_MI, MAX_MATCH_DISTANCE_IN_MI_DEFAULT_S);

		double maxMatchDistanceInMi = MAX_MATCH_DISTANCE_IN_MI_DEFAULT;

		try{
			maxMatchDistanceInMi = Double.parseDouble(maxMatchDistanceInMilesS);
		}
		catch(NumberFormatException nfe){
			maxMatchDistanceInMi = MAX_MATCH_DISTANCE_IN_MI_DEFAULT;
		}

		if(MATCH_MODE_ZCTA_ZIP_BOUNDARY_SHAPE.equalsIgnoreCase(matchMode)){
			return GeoUtil.matchWithBoundaryZcta(inputFilename, latCol, lonCol, outputFilename, inputZipS, maxMatchDistanceInMi, task);		
		}
		else if(MATCH_MODE_DISTANCE_TO_CIV_SPACE_ZIP_CENTER.equalsIgnoreCase(matchMode)){
			return matchWithMaxDistanceToCivSpaceZipcode(inputFilename, latCol, lonCol, outputFilename, inputZipS, maxMatchDistanceInMi, task);		
		}

		//MATCH_MODE_CLOSEST_CIV_SPACE_ZIP_CENTER
		return matchWithClosestCivSpaceZipcode(inputFilename, latCol, lonCol, outputFilename, inputZipS, maxMatchDistanceInMi, task);		
	}








	public static FileProcessedResultsSummary matchWithMaxDistanceToCivSpaceZipcode(
			String inputFilename, int latCol, int lonCol, String outputFilename, 
			String inputZipS, double maxMatchDistanceInMi, 
			FindInZipTask task){
		long linesLastUpdatedAt = 0;
		FileProcessedResultsSummary res = new FileProcessedResultsSummary();
		res.status = FileProcessedResultsSummary.ErrorCode.IN_PROGRESS;
		BufferedReader reader = null;
		ArrayList<LatLonData> positiveResults = new ArrayList<LatLonData>();
		ArrayList<LatLonData> negativeResults = new ArrayList<LatLonData>();

		boolean mapPosRes = ("true".equalsIgnoreCase(Props.getProps().getProperty("mapPositiveResults", "false")));
		boolean mapNegRes = ("true".equalsIgnoreCase(Props.getProps().getProperty("mapNegativeResults", "false")));

		if(maxMatchDistanceInMi < 0.0){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error invalid value used for "+MAX_MATCH_DISTANCE_IN_MI+ " in config.properties file.";
			return res;
		}

		ArrayList<CivicSpaceZipcodeData> allValidZipcodeList = null;

		try{
			allValidZipcodeList = loadAllValidZipcodes(ZIPCODE_FILENAME);
		}
		catch(FileNotFoundException fnfe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening zipcode data file: "+ ZIPCODE_FILENAME + "! " + fnfe;
			return res;
		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error reading from zipcode data file: "+ ZIPCODE_FILENAME + "! " + ioe;
			return res;
		}

		if(allValidZipcodeList == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "No zipcodes loaded from zipcode data file: "+ ZIPCODE_FILENAME + "!";
			return res;
		}

		inputZipS = inputZipS.trim();
		CivicSpaceZipcodeData inputZip = findZipcode(inputZipS, allValidZipcodeList);

		if(inputZip == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Unknown zipcode! "+ inputZipS;
			return res;
		}

		try{
			res.fileLineCount = countLinesInFile(inputFilename);
		}
		catch(FileNotFoundException fnfe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file to count lines: "+ inputFilename + "! " + fnfe;
			return res;
		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file to count lines: "+ inputFilename + "! " + ioe;
			return res;
		}

		try{
			reader = new BufferedReader(new FileReader(inputFilename));
		}
		catch(FileNotFoundException fnfe){

			try{
				if(reader != null)reader.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file: "+ inputFilename + "! " + fnfe;
			return res;
		}

		BufferedWriter writer = null;

		try{
			writer = new BufferedWriter(new FileWriter(outputFilename));
		}
		catch(IOException ioe){

			try{
				if(writer != null)writer.close();
			}
			catch(IOException cioe){
				System.out.println(cioe);
			}

			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening output stream to output file: "+ outputFilename + "! " + ioe;
			return res;
		}		

		if(inputZip == null || inputZip.getZip() == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "No valid zipcode selected!";
			return res;
		}

		try{
			String line = null;

			//skip the first line that contains the headers
			line = reader.readLine();
			res.totalNumberOfLinesProcessed++;

			//write header out to output file
			if(line != null) writer.write(line+'\n');

			while(line != null && task.getCont()){
				line = reader.readLine();

				if(line != null){
					res.totalNumberOfLinesProcessed++;
					line = line.trim();

					//just discard any blank lines
					if(line.length() != 0){
						LatLonData dataItem = new LatLonData(line, latCol, lonCol);

						if(dataItem.isValid()){

							if(isDistInMiLessThan(dataItem.getLatF(), dataItem.getLonF(), inputZip.getLatF(), inputZip.getLonF(), maxMatchDistanceInMi)){
								writer.write(dataItem.getInputString()+'\n');
								res.linesMatched++;

								if(mapPosRes){
									positiveResults.add(dataItem);
								}

							}
							else{

								if(mapNegRes){
									negativeResults.add(dataItem);
								}

							}

						}
						else{
							res.linesCouldNotParse++;
						}

					}

					if(res.totalNumberOfLinesProcessed > linesLastUpdatedAt+UPDATE_LINES_NUM){
						task.publ(new FileProcessedProgress(res));
					}

				}

			}


		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error reading from input file: "+ inputFilename + "! " + ioe;
			return res;
		}		
		finally{

			try{
				if(reader != null)reader.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

			try{
				if(writer != null)writer.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

		}

		res.status = FileProcessedResultsSummary.ErrorCode.SUCCESS;

		if(!task.getCont()){
			res.status = FileProcessedResultsSummary.ErrorCode.CANCELED;
		}

		task.publ(new FileProcessedProgress(res));

		if(mapPosRes||mapNegRes){
			GeoUtil.mapResults(positiveResults, negativeResults);
		}

		return res;
	}









	public static FileProcessedResultsSummary matchWithClosestCivSpaceZipcode(
			String inputFilename, int latCol, int lonCol, String outputFilename, 
			String inputZipS, double maxMatchDistanceInMi, 
			FindInZipTask task){
		long linesLastUpdatedAt = 0;
		FileProcessedResultsSummary res = new FileProcessedResultsSummary();
		res.status = FileProcessedResultsSummary.ErrorCode.IN_PROGRESS;
		BufferedReader reader = null;
		ArrayList<LatLonData> positiveResults = new ArrayList<LatLonData>();
		ArrayList<LatLonData> negativeResults = new ArrayList<LatLonData>();

		boolean mapPosRes = ("true".equalsIgnoreCase(Props.getProps().getProperty("mapPositiveResults", "false")));
		boolean mapNegRes = ("true".equalsIgnoreCase(Props.getProps().getProperty("mapNegativeResults", "false")));


		ArrayList<CivicSpaceZipcodeData> allValidZipcodeList = null;

		try{
			allValidZipcodeList = loadAllValidZipcodes(ZIPCODE_FILENAME);
		}
		catch(FileNotFoundException fnfe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening zipcode data file: "+ ZIPCODE_FILENAME + "! " + fnfe;
			return res;
		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error reading from zipcode data file: "+ ZIPCODE_FILENAME + "! " + ioe;
			return res;
		}

		if(allValidZipcodeList == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "No zipcodes loaded from zipcode data file: "+ ZIPCODE_FILENAME + "!";
			return res;
		}

		inputZipS = inputZipS.trim();
		CivicSpaceZipcodeData inputZip = findZipcode(inputZipS, allValidZipcodeList);

		if(inputZip == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Unknown zipcode! "+ inputZipS;
			return res;
		}

		try{
			res.fileLineCount = countLinesInFile(inputFilename);
		}
		catch(FileNotFoundException fnfe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file to count lines: "+ inputFilename + "! " + fnfe;
			return res;
		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file to count lines: "+ inputFilename + "! " + ioe;
			return res;
		}

		try{
			reader = new BufferedReader(new FileReader(inputFilename));
		}
		catch(FileNotFoundException fnfe){

			try{
				if(reader != null)reader.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening input data file: "+ inputFilename + "! " + fnfe;
			return res;
		}

		BufferedWriter writer = null;

		try{
			writer = new BufferedWriter(new FileWriter(outputFilename));
		}
		catch(IOException ioe){

			try{
				if(writer != null)writer.close();
			}
			catch(IOException cioe){
				System.out.println(cioe);
			}

			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error opening output stream to output file: "+ outputFilename + "! " + ioe;
			return res;
		}		

		if(inputZip == null || inputZip.getZip() == null){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "No valid zipcode selected!";
			return res;
		}

		try{
			String line = null;

			//skip the first line that contains the headers
			line = reader.readLine();
			res.totalNumberOfLinesProcessed++;

			//write header out to output file
			if(line != null) writer.write(line+'\n');

			while(line != null && task.getCont()){
				line = reader.readLine();

				if(line != null){
					res.totalNumberOfLinesProcessed++;
					line = line.trim();

					//just discard any blank lines
					if(line.length() != 0){
						LatLonData dataItem = new LatLonData(line, latCol, lonCol);

						if(dataItem.isValid()){

							if(isClosestToInCivSpaceZipcode(dataItem, inputZip, allValidZipcodeList, maxMatchDistanceInMi)){
								writer.write(dataItem.getInputString()+'\n');
								res.linesMatched++;

								if(mapPosRes){
									positiveResults.add(dataItem);
								}

							}
							else{

								if(mapNegRes){
									negativeResults.add(dataItem);
								}

							}

						}
						else{
							res.linesCouldNotParse++;
						}

					}

					if(res.totalNumberOfLinesProcessed > linesLastUpdatedAt+UPDATE_LINES_NUM){
						task.publ(new FileProcessedProgress(res));
					}

				}

			}


		}
		catch(IOException ioe){
			res.status = FileProcessedResultsSummary.ErrorCode.FATAL_ERROR;
			res.fatalErrorDetails = "Error reading from input file: "+ inputFilename + "! " + ioe;
			return res;
		}		
		finally{

			try{
				if(reader != null)reader.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

			try{
				if(writer != null)writer.close();
			}
			catch(IOException ioe){
				System.out.println(ioe);
			}

		}

		res.status = FileProcessedResultsSummary.ErrorCode.SUCCESS;

		if(!task.getCont()){
			res.status = FileProcessedResultsSummary.ErrorCode.CANCELED;
		}

		task.publ(new FileProcessedProgress(res));

		if(mapPosRes||mapNegRes){
			GeoUtil.mapResults(positiveResults, negativeResults);
		}

		return res;
	}

	//finds the distance to the chosen zip and loops thru all the zipcodes seeing if any are closer
	//return false if there is a closer one, true otherwise
	//allZipcodeList also contains the chosen zip so it checks against that
	private static boolean isClosestToInCivSpaceZipcode(LatLonData ll, CivicSpaceZipcodeData chosenZip, ArrayList<CivicSpaceZipcodeData> allZipcodeList, double maxMatchDistanceInMi){

		if(!chosenZip.isValid()){
			return false;
		}

		float recordDeltaLat = Math.abs(ll.getLatF()-chosenZip.getLatF());
		float recordDeltaLon = Math.abs(ll.getLonF()-chosenZip.getLonF());

		if(maxMatchDistanceInMi > 0.0 && !isDistInMiLessThan(ll.getLatF(), ll.getLonF(), chosenZip.getLatF(), chosenZip.getLonF(), maxMatchDistanceInMi)){
			 return false;
		}

		double recordDist = calcRelativeLatLonDist(ll.getLatF(), ll.getLonF(), chosenZip.getLatF(), chosenZip.getLonF());

		for(int i = 0; i < allZipcodeList.size(); i++){
			CivicSpaceZipcodeData zipi = allZipcodeList.get(i);

			if(zipi.isValid()){
				float tempDeltaLat = Math.abs(ll.getLatF()-zipi.getLatF());
				float tempDeltaLon = Math.abs(ll.getLonF()-zipi.getLonF());

				if(tempDeltaLat < recordDeltaLat && tempDeltaLon < recordDeltaLon){
					return false;
				}

				if(tempDeltaLat > recordDeltaLat && tempDeltaLon > recordDeltaLon){
					continue;
				}

				double tempDist = calcRelativeLatLonDist(ll.getLatF(), ll.getLonF(), zipi.getLatF(), zipi.getLonF());

				if(tempDist < recordDist){

					if(!(chosenZip.getZip().equals(zipi.getZip()))){
						return false;
					}

				}

			}

		}

		return true;
	}

	//comparison around atLat = lat1 in implied
	protected static boolean isDistInMiLessThan(float lat1, float lon1, float lat2, float lon2, double maxDist){
		final double safetyFactor = 1.5;
		double maxDistInDegLat = convertMiToDegLat(maxDist);
		double deltaLat = Math.abs(lat2-lat1);

		if(deltaLat > maxDistInDegLat*safetyFactor){
			return false;
		}

		double maxDistInDegLon = convertDegLatToDegLon(maxDistInDegLat, lat1);
		double deltaLon = Math.abs(lon2-lon1);

		if(deltaLon > maxDistInDegLon*safetyFactor){
			return false;
		}

		double dist = calcLatLonDistInMi(lat1, lon1, lat2, lon2);

		if(dist < maxDist){
			return true;
		}

		return false;
	}

	public static double convertMiToDegLat(double distInMi){
		return distInMi/69.047;
	}

	public static double convertDegLatToMi(double distInDegLat){
		return distInDegLat*69.047;
	}

	//converts a distance in miles to a delta in degrees of longitude if the delta is around the latitude given in atLat 
	//(at Lat needed because lon degrees get closer together near the poles)
	public static double convertMiToDegLonAtLat(double distInMi, float atLat){
		return distInMi/69.047/Math.cos(Math.toRadians(atLat));
	}

	//converts a delta in degrees of longitude to a distance in miles if the delta is around the latitude given in atLat 
	//(at Lat needed because lon degrees get closer together near the poles)
	public static double convertDegLonToMiAtLat(double distInDegLon, float atLat){
		return distInDegLon*69.047*Math.cos(Math.toRadians(atLat));
	}

	public static double convertDegLatToDegLon(double distInDegLat, float atLat){
		return distInDegLat/Math.cos(Math.toRadians(atLat));
	}

	public static double convertDegLonToDegLat(double distInDegLon, float atLat){
		return distInDegLon*Math.cos(Math.toRadians(atLat));
	}

	protected static double calcLatLonDistInMi(float lat1, float lon1, float lat2, float lon2){
		return calcRelativeLatLonDist(lat1,lon1,lat2,lon2)*3947.0f;
	}

	protected static double calcLatLonDistInKm(float lat1, float lon1, float lat2, float lon2){
		return calcRelativeLatLonDist(lat1,lon1,lat2,lon2)*6353.0f;
	}

        //the Haversine Formula calculates the great circle distance between two points on the Earth
        //the result of this function would need to be multiplied by the radius of the earth to get actual distance
	//but comparing relative distances as we do does not need that
	//multiply by 6,353 for km or 3,947 for miles 
	protected static double calcRelativeLatLonDist(float lat1, float lon1, float lat2, float lon2){
		double dLat = Math.toRadians(lat2-lat1);
		double dLon = Math.toRadians(lon2-lon1);
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2);
		double c = 2 * Math.asin(Math.sqrt(a));
		return c;
	}

	protected static boolean createTestDataFile(String filename, int count, float minLat, float maxLat, float minLon, float maxLon, long randomSeed){
		BufferedWriter writer = null;

		try{
			writer = new BufferedWriter(new FileWriter(filename));
			Random rand = new Random(randomSeed);
			writer.write("lat,long"+'\n');

			for(int i = 0; i < count; i++){
				float lat = minLat + (rand.nextFloat() * (maxLat-minLat));
				float lon = minLon + (rand.nextFloat() * (maxLon-minLon));
				LatLonData ll = new LatLonData(new String(""+lat), new String(""+lon));
				writer.write(ll.toString()+'\n');
			}

		}
		catch(IOException ioe){

			try{
				writer.close();
			}
			catch(IOException cioe){
				System.out.println(cioe);
			}

			System.out.println("Error while trying to write out to file: "+filename);
			System.out.println(ioe.toString());
			return false;
		}
		finally{

			try{
				if(writer != null) writer.close();
			}
			catch(IOException ioe){
				System.out.println("Error while trying to close file: "+filename);
				System.out.println(ioe.toString());
				return false;
			}
		
			return true;
		}

	}

	public static long countLinesInFile(String filename) throws IOException {
		long cnt = 0;
		LineNumberReader reader = null;

		try{
			reader = new LineNumberReader(new FileReader(filename));
			String lineRead = "";

			while ((lineRead = reader.readLine()) != null) {}

			cnt = reader.getLineNumber(); 
		}
		finally{
			if(reader != null)reader.close();
		}

		return cnt;
	}

	public static ArrayList<CivicSpaceZipcodeData> loadAllValidZipcodes(String zipcodeFilename) throws FileNotFoundException, IOException{
		ArrayList<CivicSpaceZipcodeData> zipList = null;
		boolean includeInvalidItems = false;
		String[] filterByZips = null;
		zipList = CivicSpaceZipcodeFileParser.parseFile(zipcodeFilename, includeInvalidItems, filterByZips);
		return zipList;
	}

	private static CivicSpaceZipcodeData findZipcode(String inputZipS, ArrayList<CivicSpaceZipcodeData> allValidZipcodeList){

		if(inputZipS == null){
			return null;
		}

		for(int i = 0; i < allValidZipcodeList.size(); i++){
			CivicSpaceZipcodeData zipi = allValidZipcodeList.get(i);

			if(inputZipS.equals(zipi.getZip())){
				return zipi;
			}

		}

		return null;
	}
		
}