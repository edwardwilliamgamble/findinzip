import java.math.BigDecimal;

public class FileProcessedResultsSummary{

	public enum ErrorCode {
		NOT_STARTED, IN_PROGRESS, CANCELED, SUCCESS, FATAL_ERROR
	}

	ErrorCode status = ErrorCode.NOT_STARTED;
	String fatalErrorDetails = "";
	long linesMatched = 0l;
	long linesCouldNotParse = 0l;
	long totalNumberOfLinesProcessed = 0l;
	long fileLineCount = -1l;

	public FileProcessedResultsSummary(){
	}

	public FileProcessedResultsSummary(ErrorCode status, String fatalErrorDetails, 
			long linesMatched, long linesCouldNotParse,
			long totalNumberOfLinesProcessed, long fileLineCount){
		this.status = status;
		this.fatalErrorDetails = fatalErrorDetails;
		this.linesMatched = linesMatched;
		this.linesCouldNotParse = linesCouldNotParse;
		this.totalNumberOfLinesProcessed = totalNumberOfLinesProcessed;
		this.fileLineCount = fileLineCount;
	}

	public String toString(){

		if(fileLineCount < 0){
			return "Lines processed: "+totalNumberOfLinesProcessed+", "+ getCompletedPercentDesc(totalNumberOfLinesProcessed,fileLineCount) +"%, Matched: "+linesMatched+", Malformed input lines: "+linesCouldNotParse;
		}
		else{
			return "Lines processed: "+totalNumberOfLinesProcessed+" / " + fileLineCount + ", "+ getCompletedPercentDesc(totalNumberOfLinesProcessed,fileLineCount) +"%, Matched: "+linesMatched+", Malformed input lines: "+linesCouldNotParse;
		}

	}

	public static String getCompletedPercentDesc(long numer, long denom){
		BigDecimal dn = new BigDecimal(""+numer);
		BigDecimal dd = new BigDecimal(""+denom);
		BigDecimal bret = dn.multiply(new BigDecimal("100.00")).divide(dd, BigDecimal.ROUND_UP);
		float f = bret.floatValue();

		//don't show 100.00% until we are completely done
		if(f == 100.00 && numer < denom) f = 99.99f;

		return String.format("%6.2f",f);
	}

}