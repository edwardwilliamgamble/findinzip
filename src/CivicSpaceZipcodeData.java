public class CivicSpaceZipcodeData{
	private String inputString = null;
	private String zip = null;
	private String city = null;
	private String stateAbbr = null;
	private String latS = null;
	private String lonS = null;
	private float latF = -9999.0f;
	private float lonF = -9999.0f;
	private String timezone = null;
	private String usesDst = null;
	private boolean isValid = false; 

	public CivicSpaceZipcodeData(String inputString){
		parse(inputString);
	}

	private void parse(String inputString){
		isValid = false;

		if(inputString != null){
			this.inputString = inputString;
			String[] tokens = inputString.split(",", 7);

			if(tokens.length == 7){
				this.zip = tokens[0].replaceAll("\"","").trim();
				this.city = tokens[1].replaceAll("\"","").trim();
				this.stateAbbr = tokens[2].replaceAll("\"","").trim();
				this.latS = tokens[3].replaceAll("\"","").trim();
				this.lonS = tokens[4].replaceAll("\"","").trim();
				this.timezone = tokens[5].replaceAll("\"","").trim();
				this.usesDst = tokens[6].replaceAll("\"","").trim();
				parse(this.zip, this.city, this.stateAbbr, this.latS, this.lonS, this.timezone, this.usesDst);
			}

		}
	}

	public CivicSpaceZipcodeData(String zip, String city, String stateAbbr, String latS, String lonS, String timezone, String usesDst){
		parse(zip, city, stateAbbr, latS, lonS, timezone, usesDst);
	}

	private void parse(String zip, String city, String stateAbbr, String latS, String lonS, String timezone, String usesDst){

		if(zip == null){
			isValid = false;
			return;
		}

		if(latS == null){
			isValid = false;
			return;
		}

		if(lonS == null){
			isValid = false;
			return;
		}

		if(city == null){
			city = "";
		}

		if(stateAbbr == null){
			stateAbbr = "";
		}

		if(timezone == null){
			timezone = "";
		}

		if(usesDst == null){
			usesDst = "";
		}

		this.zip = zip;
		this.city = city;
		this.stateAbbr = stateAbbr;
		this.latS = latS;
		this.lonS = lonS;
		this.timezone = timezone;
		this.usesDst = usesDst;
		this.isValid = true;

		try{
			this.latF = Float.parseFloat(this.latS);
			this.lonF = Float.parseFloat(this.lonS);
			Integer.parseInt(zip);
		}
		catch(NumberFormatException nfe){
			this.isValid = false;
		}

		if(this.isValid && 
			Float.isNaN(this.latF)
			|| Float.isInfinite(this.latF) 
			|| this.latF < -90.0f 
			|| this.latF > 90.0f
			|| Float.isNaN(this.lonF)
			|| Float.isInfinite(this.lonF) 
			|| this.lonF < -180.0f
			|| this.lonF > 180.0f
			|| this.zip.length() != 5
		){
			this.isValid = false;
		}

	}

	public boolean isValid(){
		return isValid;
	}

	public String getInputString(){
		return inputString;
	}

	public String getZip(){
		return zip;
	}

	public String getCity(){
		return city;
	}

	public String getStateAbbr(){
		return stateAbbr;
	}

	public String getLatS(){
		return latS;
	}

	public String getLonS(){
		return lonS;
	}

	public float getLatF(){
		return latF;
	}

	public float getLonF(){
		return lonF;
	}

	public String getTimezone(){
		return timezone;
	}

	public String getUsesDst(){
		return usesDst;
	}

	public String toString(){
		String ret = "";

		if(isValid()){
			ret = ""+zip+","+city+","+stateAbbr+","+latS+","+lonS+","+timezone+","+usesDst;
		}
		else if(inputString != null){
			ret = "<Invalid Civic Space zipcode data item:"+inputString+">";
		}
		else{
			ret = "<Invalid Civic Space zipcode data item with no input values>";
		}

		return ret;
	}

}
