import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.util.concurrent.*;

public class Gui{
	private static final String TITLE = "Find In Zip";
	private static final String DEFAULT_INPUT_FILENAME = "inputdata.csv";
	private static final String DEFAULT_LAT_COL = "0";
	private static final String DEFAULT_LON_COL = "1";
	private static final String DEFAULT_OUTPUT_FILENAME = "outputdata.csv";
	private static final String ICON_IMAGE_FILENAME = ".\\icon.png";
	public JFrame frame;
	public JButton cancelButton;
	public JButton runButton;
	private FindInZipTask task = null;

	public Gui(){
	}

	public void show(){

		//Schedule a job for execution on the swing event-dispatching thread:
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});

	}

	private void createAndShowGui(){
		frame = new JFrame(TITLE);
		frame.setLocation(100, 100);
		ImageIcon img = new ImageIcon(ICON_IMAGE_FILENAME);
		frame.setIconImage(img.getImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container contentPane = frame.getContentPane();
		contentPane.setLayout(new BorderLayout());

		JPanel fieldsPanel = new JPanel();
		fieldsPanel.setLayout(new SpringLayout());
		contentPane.add(fieldsPanel, BorderLayout.PAGE_START);

		JLabel inputFilenameLabel = new JLabel("Input file name: ", JLabel.TRAILING);
		fieldsPanel.add(inputFilenameLabel);
		final JTextField inputFilenameTextField = new JTextField(DEFAULT_INPUT_FILENAME, 50);
		inputFilenameLabel.setLabelFor(inputFilenameTextField);
		fieldsPanel.add(inputFilenameTextField);

		JLabel zipcodeLabel = new JLabel("U.S. zipcode: ", JLabel.TRAILING);
		fieldsPanel.add(zipcodeLabel);
		final JTextField zipcodeTextField = new JTextField("", 5);
		zipcodeLabel.setLabelFor(zipcodeTextField);
		fieldsPanel.add(zipcodeTextField);

		JLabel latColLabel = new JLabel("Lat Column #: ", JLabel.TRAILING);
		fieldsPanel.add(latColLabel);
		final JTextField latColTextField = new JTextField(DEFAULT_LAT_COL, 4);
		latColLabel.setLabelFor(latColTextField);
		fieldsPanel.add(latColTextField);

		JLabel lonColLabel = new JLabel("Lon Column #: ", JLabel.TRAILING);
		fieldsPanel.add(lonColLabel);
		final JTextField lonColTextField = new JTextField(DEFAULT_LON_COL, 4);
		lonColLabel.setLabelFor(lonColTextField);
		fieldsPanel.add(lonColTextField);

		JLabel outputFilenameLabel = new JLabel("Output file name: ", JLabel.TRAILING);
		fieldsPanel.add(outputFilenameLabel);
		final JTextField outputFilenameTextField = new JTextField(DEFAULT_OUTPUT_FILENAME, 50);
		outputFilenameLabel.setLabelFor(outputFilenameTextField);
		fieldsPanel.add(outputFilenameTextField);

		int numPairs = 5;
		makeCompactGrid(fieldsPanel,
			numPairs, 2,	//rows, cols
			6, 6,		//initX, initY
			6, 6);		//xPad, yPad

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout());
		contentPane.add(buttonsPanel, BorderLayout.PAGE_END);

		cancelButton = new JButton("Cancel");
		buttonsPanel.add(cancelButton);
		runButton = new JButton("Run");
		buttonsPanel.add(runButton);

		runButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				runButton.setEnabled(false);
				task = new FindInZipTask(Gui.this, inputFilenameTextField.getText().trim(), latColTextField.getText().trim(), lonColTextField.getText().trim(), outputFilenameTextField.getText().trim(), zipcodeTextField.getText().trim());
				task.execute();
				cancelButton.setEnabled(true);
			}
		});

		runButton.setEnabled(true);
		cancelButton.setEnabled(false);

		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				runButton.setEnabled(false);

				if(task != null){
					task.setCont(false);
					task = null;
				}

				cancelButton.setEnabled(true);
			}
		});

		frame.pack();
		frame.setVisible(true);
	}

	private static SpringLayout.Constraints getConstraintsForCell(
			int row, int col,
			Container parent,
			int cols){
		SpringLayout layout = (SpringLayout) parent.getLayout();
		Component c = parent.getComponent(row * cols + col);
		return layout.getConstraints(c);
	}

	public static void makeCompactGrid(Container parent,
			int rows, int cols,
			int initialX, int initialY,
			int xPad, int yPad){
		SpringLayout layout;

		try{
			layout = (SpringLayout)parent.getLayout();
		}
		catch (ClassCastException exc){
        		System.err.println("The first argument to makeCompactGrid must use SpringLayout.");
			return;
		}

		//Align all cells in each column and make them the same width.
		Spring x = Spring.constant(initialX);

		for (int c = 0; c < cols; c++) {
			Spring width = Spring.constant(0);

			for (int r = 0; r < rows; r++) {
				width = Spring.max(width, getConstraintsForCell(r, c, parent, cols).getWidth());
			}

			for (int r = 0; r < rows; r++) {
				SpringLayout.Constraints constraints = getConstraintsForCell(r, c, parent, cols);
				constraints.setX(x);
				constraints.setWidth(width);
			}

			x = Spring.sum(x, Spring.sum(width, Spring.constant(xPad)));
		}

		//Align all cells in each row and make them the same height.
		Spring y = Spring.constant(initialY);

		for (int r = 0; r < rows; r++) {
			Spring height = Spring.constant(0);

			for (int c = 0; c < cols; c++) {
				height = Spring.max(height, getConstraintsForCell(r, c, parent, cols).getHeight());
            		}

			for (int c = 0; c < cols; c++) {
				SpringLayout.Constraints constraints = getConstraintsForCell(r, c, parent, cols);
				constraints.setY(y);
				constraints.setHeight(height);
			}

			y = Spring.sum(y, Spring.sum(height, Spring.constant(yPad)));
		}

		//Set the parent's size.
		SpringLayout.Constraints pCons = layout.getConstraints(parent);
		pCons.setConstraint(SpringLayout.SOUTH, y);
		pCons.setConstraint(SpringLayout.EAST, x);
	}

}